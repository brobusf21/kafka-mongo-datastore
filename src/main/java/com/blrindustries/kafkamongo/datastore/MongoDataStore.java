package com.blrindustries.kafkamongo.datastore;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.and;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.WriteModel;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
public class MongoDataStore {
    @Autowired
    private MongoDatabase mongoDatabase;

    @Autowired
    private Map<String, String> collectionMap;

    @Autowired
    private CounterService counterService;

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDataStore.class);
    private static final BulkWriteOptions BULK_WRITE_OPTIONS = new BulkWriteOptions().ordered(false);

    public void save(String topicName, Collection<String> records){
        List<? extends WriteModel<Document>> docsToWrite = buildInsertModel(topicName, records);
        BulkWriteResult result = null;

        try {
            result = mongoDatabase.getCollection(collectionMap.get(topicName)).bulkWrite(docsToWrite,
                    BULK_WRITE_OPTIONS);
            LOGGER.info("Inserted number of documents: " + result.getInsertedCount());
            counterService.increment("Mongo Collection " + result.getInsertedCount() + ".saved" );
        } catch (Exception e){
            LOGGER.warn("Bulk Write to MongoDB error" + e);
            counterService.increment("Mongo Collection " + ".error" );
        }
    }

    public void deleteExistingDealerRecords(String topicName, String record) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        long dateInMilliSec = new Date().getTime();
        Date isoDateWithCurrentTime = new Date(dateInMilliSec);
        String isoDateWithStaticTimeStr = dateFormat.format(isoDateWithCurrentTime).
                replaceAll("[0-9]{1,2}:[0-9]{1,2}(:[0-9][0-9])?", "00:00:00");
        Document doc = documentBuilder(record);

        try {
            Date isoDateWithStaticTime = dateFormat.parse(isoDateWithStaticTimeStr);
            long deletedCounter = mongoDatabase.getCollection(collectionMap.get(topicName)).deleteMany(
                    and(eq("DealerCode", doc.get("DealerCode")),
                            lt("CreatedAt", isoDateWithStaticTime))).getDeletedCount();
            LOGGER.info("Deleted {} existing records for DealerCode = {}", deletedCounter, doc.get("DealerCode"));
        } catch (Exception e) {
            LOGGER.error("Error within deleteExistingDealerRecords; DealerCode = {}", doc.get("DealerCode"));
        }
    }

    private List<? extends WriteModel<Document>> buildInsertModel(String topicName, Collection<String> records) {
        List<InsertOneModel<Document>> docsToInsert = new ArrayList<>(records.size());
        records.forEach(record -> {
                    Document doc = documentBuilder(record);
                    Document docWithTimeStamp = appendTimeStampToDoc(doc);
                    docsToInsert.add(new InsertOneModel<Document>(docWithTimeStamp));
                }
        );
        return docsToInsert;
    }

    private Document documentBuilder(String record) {
        Map<String, Object> jsonMap = new HashMap<>();

        try {
            jsonMap = SchemaUtils.toJsonMap(record);
        }
        catch (IOException e) {
            LOGGER.warn("Json to Document map error" + e);
        }
        // DEBUG jsonMap.forEach((k,v)->LOGGER.info("k: " + k + " v: " + v));
        Document doc = new Document(jsonMap);
        return doc;
    }

    private Document appendTimeStampToDoc(Document doc) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        long dateInMilliSec = new Date().getTime();
        Date isoDateWithCurrentTime = new Date(dateInMilliSec);
        String isoDateStaticTimeStr = dateFormat.format(isoDateWithCurrentTime).
                replaceAll("[0-9]{1,2}:[0-9]{1,2}(:[0-9][0-9])?", "00:00:00");
        try {
            Date isoDateStaticTime = dateFormat.parse(isoDateStaticTimeStr);
            // DEBUG Used to append any date --> Date insertionDate = dateFormat.parse("2018-01-09T00:00:00");
            doc.put("CreatedAt", isoDateStaticTime);
        } catch (ParseException e) {
            LOGGER.warn("ParseException thrown appending time stamp to record before insertion");
        }
        return doc;
    }
}
