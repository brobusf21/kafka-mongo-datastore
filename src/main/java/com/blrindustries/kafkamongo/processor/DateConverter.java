package com.blrindustries.kafkamongo.processor;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateConverter {

    private static final Logger LOGGER              = LoggerFactory.getLogger(DateConverter.class);
    private static final String MONTH_DAY_YEAR      = "MM/dd/yy";
    private static final String DAY_MONTH_YEAR      = "dd/MM/yyyy";
    private static final String YEAR_MONTH_DAY      = "yyyy-MM-dd";
    private static final String REGULAR_FORMAT      = "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})";
    private static final String ISO_FORMAT          = "([0-9]{4})-([0-9]{2})-([0-9]{2})";
    private static final String REMOVE_TIME         = "[0-9]{1,2}:[0-9]{1,2}(:[0-9][0-9])?";
    private static final String SALES               = "sales.oneeighty.vehiclesales";
    private static final String INVENTORY           = "vehicle.oneeighty.vehicleinventory";

    private SimpleDateFormat monthDayYearDateFormat, dayMonthYearDateFormat, yearMonthDayDateFormat;
    private Pattern regDatePattern;
    private Pattern isoDatePattern;
    private String topicName;
    private String jsonString;

    public DateConverter(String jsonString, String topicName) {
        this.jsonString = jsonString;
        this.topicName = topicName;
        regDatePattern = Pattern.compile(REGULAR_FORMAT);
        isoDatePattern = Pattern.compile(ISO_FORMAT);
        monthDayYearDateFormat = new SimpleDateFormat(MONTH_DAY_YEAR);
        dayMonthYearDateFormat = new SimpleDateFormat(DAY_MONTH_YEAR);
        yearMonthDayDateFormat = new SimpleDateFormat(YEAR_MONTH_DAY);
    }

    public String processConversion() {
        if (topicName.equals(SALES))
            return salesTopicDateConversion();
        else
            return inventoryTopicDateConversion();
    }

    private String salesTopicDateConversion() {
        String dateStr;
        Matcher dateMatcher;
        Date d;
        JSONObject jsonRecord = new JSONObject(jsonString.replaceAll(REMOVE_TIME, ""));
        long milliseconds;

        for (Object key : jsonRecord.keySet()) {
            String keyStr = (String) key;
            Object keyValue = jsonRecord.get(keyStr);

            if (keyStr.equals("DealerCode") && isNullValue(keyValue)) { // DealerCode is REQUIRED for all sales records
                LOGGER.error("DealerCode can not be null, therefore we can assume it is a corrupted record, skipping");
                return null;
            }

            if (isNullValue(keyValue)) continue; // No need to try and convert null values, skip that column entry

            if (keyStr.contains("Date")) {
                dateMatcher = regDatePattern.matcher((String) keyValue);
                if (dateMatcher.find()) {
                    dateStr = dateMatcher.group(0);
                } else {
                    LOGGER.warn("Date value is not a valid date and is not null! Assuming message is corrupted.");
                    return null;
                }
                try {
                    d = monthDayYearDateFormat.parse(dateStr);
                    milliseconds = d.getTime();
                } catch (ParseException e) {
                    LOGGER.warn("ParseException within sales record, dateStr: " + dateStr + " , message: "+ e);
                    return null;
                } catch (NullPointerException e) {
                    LOGGER.warn("NullPointerException within sales record, dateStr: " + dateStr + " , message: " + e);
                    return null;
                }
                jsonRecord.put(keyStr, milliseconds);
            }
        }
        LOGGER.info("topicName: " + topicName + " dateConverter returning: " + jsonRecord.toString());
        return jsonRecord.toString();
    }

    private String inventoryTopicDateConversion() {
        String dateStr;
        Matcher dateMatcher;
        Date actualDate;
        JSONObject jsonRecord = new JSONObject(jsonString.replaceAll(REMOVE_TIME, ""));
        long milliseconds;

        for (Object key: jsonRecord.keySet()) {
            String keyStr = (String) key;
            Object keyValue = jsonRecord.get(keyStr);

            if (keyStr.equals("DealerId") && isNullValue(keyValue)) { // DealerId is REQUIRED for all inventory records
                LOGGER.error("DealerId can not be null, therefore we can assume it is a corrupted record");
                return null;
            }

            if (isNullValue(keyValue)) continue; // No need to try and convert null values, skip that column entry

            if (keyStr.equals("SoldDate") || keyStr.equals("InServiceDate")) {
                dateMatcher = regDatePattern.matcher((String) keyValue);
                if (dateMatcher.find()) {
                    dateStr = dateMatcher.group(0);
                } else {
                    LOGGER.error("Value is not a date, see the key and value pair: {} : {}", keyStr, keyValue);
                    return null;
                }
                try {
                    actualDate = monthDayYearDateFormat.parse(dateStr);
                    milliseconds = actualDate.getTime();
                } catch (ParseException e) {
                    LOGGER.error("ParseException within monthDayYearFormat parser of inventory: " + e);
                    return null;
                }
                jsonRecord.put(keyStr, milliseconds);
            } else if (keyStr.equals("PhotoChangeDate_Pipe") || keyStr.equals("ArrivalDate") ||
                       keyStr.equals("PhotoCurrentDate_Pipe2") || keyStr.equals("LastModified") ||
                       keyStr.equals("CurrentDate")) {
                StringBuilder newKeyValue = new StringBuilder();
                dateMatcher = isoDatePattern.matcher((String) keyValue);
                while (dateMatcher.find()) {
                    dateStr = dateMatcher.group();
                    try {
                        actualDate = yearMonthDayDateFormat.parse(dateStr);
                        milliseconds = actualDate.getTime();
                    } catch (ParseException e) {
                        LOGGER.error("ParseException within yearMonthDayFormat parser of inventory: " + e);
                        return null;
                    }
                    newKeyValue.append(milliseconds);
                    newKeyValue.append("|");
                }
                if (newKeyValue.length() > 0) {
                    newKeyValue.deleteCharAt(newKeyValue.length() - 1);
                }
                jsonRecord.put(keyStr, newKeyValue);
            } else if (keyStr.equals("PhotoLastModifiedDatePipe") || keyStr.equals("PhotoCurrentDate_Pipe1")
                    || keyStr.equals("CurrentDatePhotoLastModified")) {
                StringBuilder newKeyValue = new StringBuilder();
                dateMatcher = regDatePattern.matcher((String) keyValue);
                while (dateMatcher.find()) {
                    dateStr = dateMatcher.group();
                    try {
                        actualDate = dayMonthYearDateFormat.parse(dateStr);
                        milliseconds = actualDate.getTime();
                    } catch (ParseException e) {
                        LOGGER.error("ParseException within third if block of inventory: " + e);
                        return null;
                    }
                    newKeyValue.append(milliseconds);
                    if (keyStr.equals("CurrentDatePhotoLastModified")) {
                        newKeyValue.append(",");
                    } else {
                        newKeyValue.append("|");
                    }
                }
                if (newKeyValue.length() > 0) {
                    newKeyValue.deleteCharAt(newKeyValue.length() - 1);
                }
                jsonRecord.put(keyStr, newKeyValue);
            }
        }
        LOGGER.info("topicName: " + topicName + " dateConverter returning: " + jsonRecord.toString());
        return jsonRecord.toString();
    }

    private boolean isNullValue(Object keyValue) {
        if (keyValue.equals(null) || keyValue.equals(0) || keyValue.equals("") || Objects.isNull(keyValue)) {
            return true;
        }
        return false;
    }
}
