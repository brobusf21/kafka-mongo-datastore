package com.blrindustries.kafkamongo.processor;

import com.blrindustries.kafkamongo.datastore.MongoDataStore;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;

public class MessageProcessor implements Processor<String, ByteBuffer> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageProcessor.class);

    private ProcessorContext context;
    private final String topicName;
    private final GaugeService gaugeService;
    private final CounterService counterService;
    private final SchemaClient schemaClient;
    private final PayloadConverter payloadConverter = new PayloadConverter();
    private MongoDataStore mongoDataStore;
    private static int messageProcessorCounter = 0;


    public MessageProcessor(String topicName,
                            GaugeService gaugeService,
                            CounterService counterService,
                            String furl,
                            String token,
                            MongoDataStore mongoDataStore){
        this.topicName = topicName;
        this.gaugeService = gaugeService;
        this.counterService = counterService;
        this.mongoDataStore = mongoDataStore;
        schemaClient = new CachingSchemaClient(SpringSchemaClient.newInstance(furl, token));
    }


    @Override
    public void init(ProcessorContext context) {
        this.context = context;
        LOGGER.info("initialized MessageProcessor for {}",topicName);
    }

    @Override
    public synchronized void process(String key, ByteBuffer value) {
        LOGGER.info("Processing on {}|{}: {} at {}",context.topic(),context.partition(),key, Thread.currentThread());
        counterService.increment(topicName+".process");
        int recordPartition = context.partition();
        long recordOffset = context.offset();
        int version;
        try {
            version = PayloadConverter.extractCamusSchemaId(value.array());
        } catch (Exception e) {
            LOGGER.error("Failed to extract schema ID, doesn't appear to be a CAMUS style Avro message: "
                    + e.getMessage(), e);
            counterService.increment(topicName+".skipped");
            return;
        }

        Schema schema;
        int tryNum = 0;
        while (true) {
            tryNum++;
            try {
                schema = schemaClient.getSchemaByNameAndVersion(topicName, version).orElse(null);
                break;
            } catch (IOException e) {
                LOGGER.error("IOException getting schema for topic {}, version {}, on try {}. Will retry.",
                        topicName, version, tryNum, e);
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.warn("Interrupted exception being thrown while trying to get schema, topicname: " + topicName
                + " , message: " + e );
            }
        }

        if (schema == null) {
            LOGGER.error("Couldn't find schema for topic {}, version {}, skipping message.", topicName, version);
            counterService.increment(topicName+".skipped");
            return;
        }

        String rec;
        try {
            rec = payloadConverter.fromCamusPayload(schema, value.array()).toString();
            LOGGER.info("Version " + version + "; Record: " + rec);
        } catch (Exception e) {
            LOGGER.error("Skipping message on failure to convert Avro payload for topic {}, version {}, contents:\n{}",
                    topicName, version, value.array(), e);
            counterService.increment(topicName+".skipped");
            return;
        }

        if (rec == null){
            LOGGER.warn("Skipping blank message for topic {}, version {}, payload {}", topicName, version,
                    value.array());
            counterService.increment(topicName+".skipped");
            return;
        }

        try {
            rec = new DateConverter(rec, topicName).processConversion();
            if (rec == null){
                LOGGER.error("Skipping corrupted message for topic {}, version {}, record {}", topicName, version, rec);
                counterService.increment(topicName+".skipped");
                return;
            }
            mongoDataStore.deleteExistingDealerRecords(topicName, rec);
            mongoDataStore.save(topicName, Collections.singletonList(rec));
            context.commit();
            recordCommit(recordPartition, recordOffset);
            messageProcessorCounter++;
            LOGGER.info("messageProcessorCounter: " + messageProcessorCounter);
        } catch (Exception e){
            LOGGER.error("Error saving message to mongodb: {}", e.getMessage(), e);
            counterService.increment(topicName+".skipped");
        }
    }

    @Override
    public void punctuate(long timestamp) {    }

    @Override
    public void close() {    }

    private void recordCommit( int partition, long offset) {
        gaugeService.submit(getGaugeOffsetName(partition), Long.valueOf(offset));
        counterService.increment(topicName+"["+partition+"]");
        counterService.increment(topicName+".committed");
    }

    private String getGaugeOffsetName(int partition) {
        return topicName+"["+partition+"].committed.offset";
    }
}