package com.blrindustries.kafkamongo.consumer;

import com.blrindustries.kafkamongo.datastore.MongoDataStore;
import com.blrindustries.kafkamongo.processor.MessageProcessor;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;

import java.util.Properties;

public class MessageConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumer.class);

    private final String topicName;
    private final GaugeService gaugeService;
    private final CounterService counterService;
    private final String furl;
    private final String token;
    private final MongoDataStore mongoDataStore;

    private KafkaStreams streams;

    public MessageConsumer(String topicName,
                           GaugeService gaugeService,
                           CounterService counterService,
                           String furl,
                           String token,
                           MongoDataStore mongoDataStore){
        this.topicName = topicName;
        this.gaugeService = gaugeService;
        this.counterService = counterService;
        this.furl = furl;
        this.token = token;
        this.mongoDataStore = mongoDataStore;
    }

    public void consumeMessages(Properties streamProperties) {
        LOGGER.info("starting KStreamBuilder for " + topicName);
        KStreamBuilder builder = new KStreamBuilder();

        String sourceName = topicName + "-topic";
        builder.addSource(sourceName, topicName)
                .addProcessor(topicName + "-processor",
                              () -> new MessageProcessor(topicName,
                                      gaugeService,
                                      counterService,
                                      furl,
                                      token,
                                      mongoDataStore),
                              sourceName);

        streams = new KafkaStreams(builder, streamProperties);
        LOGGER.info("Starting stream for "+sourceName);
        streams.start();

    }

    public void stopConsume(){
        streams.close();
        streams.cleanUp();
    }
}
