package com.blrindustries.kafkamongo;


import com.blrindustries.kafkamongo.configuration.ConsumerConfigurationProperties;
import com.blrindustries.kafkamongo.configuration.OdpConfigurationProperties;
import com.blrindustries.kafkamongo.consumer.MessageConsumer;
import com.blrindustries.kafkamongo.datastore.MongoDataStore;
import org.apache.kafka.streams.StreamsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@SpringBootApplication
@ComponentScan(basePackages = {"com.blrindustries.kafkamongo"})

public class KafkaMongoDataStoreApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMongoDataStoreApplication.class);

	public static void main(String[] args) throws Exception {
		SpringApplication.run(KafkaMongoDataStoreApplication.class, args);
	}

	@Autowired
	private Properties streamProperties;

	@Autowired
	private List<String> topics;

	@Autowired
	private GaugeService gaugeService;

	@Autowired
	private CounterService counterService;

	@Autowired
	private ConsumerConfigurationProperties consumerProps;

	@Autowired
	private OdpConfigurationProperties odpProps;

	@Autowired
	private MongoDataStore mongoDataStore;

	private ConcurrentMap<String, MessageConsumer> consumerConcurrentMap = new ConcurrentHashMap<>();

	@PostConstruct
	public void startConsuming() {
		for (String topic : topics) {
			streamProperties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG,
					consumerProps.getApplicationId()+ "-" + topic);
			MessageConsumer consumer = new MessageConsumer(topic,
					gaugeService,
					counterService,
					odpProps.getUrl(),
					odpProps.getToken(),
					mongoDataStore);
			LOGGER.info("Consuming messages from topic '{}'", topic);
			consumerConcurrentMap.put(topic, consumer);
			consumer.consumeMessages(streamProperties);

		}
	}

	@PreDestroy
	public void endConsuming(){
		LOGGER.info("calling PreDestroy");
		consumerConcurrentMap.values().forEach(MessageConsumer::stopConsume);
	}
}
