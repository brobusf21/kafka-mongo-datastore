package com.blrindustries.kafkamongo.configuration;

import com.blrindustries.kafkamongo.datastore.MongoDataStore;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.streams.StreamsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.List;
import java.util.Properties;


@Configuration
public class ConsumerConfiguration {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerConfiguration.class);

	@Autowired
	private ConsumerConfigurationProperties consumerConfigurationProperties;

	@Autowired
	private OdpConfigurationProperties odpProps;

	@Bean
	public MongoDataStore mongoDataStore() { return new MongoDataStore();}

	@Bean
	public PayloadConverter payloadConverter() {
		return new PayloadConverter();
	}

	@Bean
	public Properties streamProperties() {
		Properties props = new Properties();
		
		putProperty(props, StreamsConfig.APPLICATION_ID_CONFIG, consumerConfigurationProperties.getApplicationId());
		putProperty(props, StreamsConfig.APPLICATION_ID_DOC, consumerConfigurationProperties.getApplicationIdDoc());
		putProperty(props, StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, consumerConfigurationProperties.getBootstrapServersConfig());
		putProperty(props, StreamsConfig.CLIENT_ID_CONFIG, consumerConfigurationProperties.getClientIdConfig());
		putProperty(props, StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, consumerConfigurationProperties.getClientIdConfig());
		putProperty(props, StreamsConfig.KEY_SERDE_CLASS_CONFIG, consumerConfigurationProperties.getKeySerdeClassConfig());
		putProperty(props, StreamsConfig.KEY_SERDE_CLASS_DOC, consumerConfigurationProperties.getKeySerdeClassDoc());
		putProperty(props, StreamsConfig.METRIC_REPORTER_CLASSES_CONFIG, consumerConfigurationProperties.getMetricReporterClassesConfig());
		putProperty(props, StreamsConfig.METRICS_NUM_SAMPLES_CONFIG, consumerConfigurationProperties.getMetricsNumSamplesConfig());
		putProperty(props, StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG, consumerConfigurationProperties.getMetricsSampleWindowMsConfig());
		putProperty(props, StreamsConfig.NUM_STANDBY_REPLICAS_CONFIG, consumerConfigurationProperties.getNumStandbyReplicasConfig());
		putProperty(props, StreamsConfig.NUM_STREAM_THREADS_CONFIG, consumerConfigurationProperties.getNumStreamThreadsConfig());
		putProperty(props, StreamsConfig.PARTITION_GROUPER_CLASS_CONFIG, consumerConfigurationProperties.getPartitionGrouperClassConfig());
		putProperty(props, StreamsConfig.POLL_MS_CONFIG, consumerConfigurationProperties.getPollMsConfig());
		putProperty(props, StreamsConfig.REPLICATION_FACTOR_CONFIG, consumerConfigurationProperties.getReplicationFactorConfig());
		putProperty(props, StreamsConfig.REPLICATION_FACTOR_DOC, consumerConfigurationProperties.getReplicationFactorDoc());
		putProperty(props, StreamsConfig.STATE_CLEANUP_DELAY_MS_CONFIG, consumerConfigurationProperties.getStateCleanupDelayMsConfig());
		putProperty(props, StreamsConfig.STATE_DIR_CONFIG, consumerConfigurationProperties.getStateDirConfig());
		putProperty(props, StreamsConfig.TIMESTAMP_EXTRACTOR_CLASS_CONFIG, consumerConfigurationProperties.getTimestampExtractorClassConfig());
		putProperty(props, StreamsConfig.VALUE_SERDE_CLASS_CONFIG, consumerConfigurationProperties.getValueSerdeClassConfig());
		putProperty(props, StreamsConfig.VALUE_SERDE_CLASS_DOC, consumerConfigurationProperties.getValueSerdeClassDoc());
		putProperty(props, StreamsConfig.ZOOKEEPER_CONNECT_CONFIG, consumerConfigurationProperties.getZookeeperConnectConfig());
		
		putProperty(props, ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, consumerConfigurationProperties.getAutoCommitIntervalMsConfig());
		putProperty(props, ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, consumerConfigurationProperties.getAutoOffsetResetConfig());
		putProperty(props, ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, consumerConfigurationProperties.getEnableAutoCommitConfig());
		putProperty(props, ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, consumerConfigurationProperties.getFetchMaxWaitMsConfig());
		putProperty(props, ConsumerConfig.FETCH_MIN_BYTES_CONFIG, consumerConfigurationProperties.getFetchMinBytesConfig());
		putProperty(props, ConsumerConfig.GROUP_ID_CONFIG, consumerConfigurationProperties.getGroupIdConfig());
		putProperty(props, ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, consumerConfigurationProperties.getHeartbeatIntervalMsConfig());
		putProperty(props, ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG, consumerConfigurationProperties.getInterceptorClassesConfig());
		putProperty(props, ConsumerConfig.INTERCEPTOR_CLASSES_DOC, consumerConfigurationProperties.getInterceptorClassesDoc());
		putProperty(props, ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, consumerConfigurationProperties.getKeyDeserializerClassConfig());
		putProperty(props, ConsumerConfig.KEY_DESERIALIZER_CLASS_DOC, consumerConfigurationProperties.getKeyDeserializerClassDoc());
		putProperty(props, ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG, consumerConfigurationProperties.getMaxPartitionFetchBytesConfig());
		putProperty(props, ConsumerConfig.MAX_POLL_RECORDS_CONFIG, consumerConfigurationProperties.getMaxPollRecordsConfig());
		putProperty(props, ConsumerConfig.METADATA_MAX_AGE_CONFIG, consumerConfigurationProperties.getMetadataMaxAgeConfig());
		putProperty(props, ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, consumerConfigurationProperties.getPartitionAssignmentStrategyConfig());
		putProperty(props, ConsumerConfig.RECEIVE_BUFFER_CONFIG, consumerConfigurationProperties.getReceiveBufferConfig());
		putProperty(props, ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG, consumerConfigurationProperties.getReconnectBackoffMsConfig());
		putProperty(props, ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, consumerConfigurationProperties.getRequestTimeoutMsConfig());
		putProperty(props, ConsumerConfig.RETRY_BACKOFF_MS_CONFIG, consumerConfigurationProperties.getRetryBackoffMsConfig());
		putProperty(props, ConsumerConfig.SEND_BUFFER_CONFIG, consumerConfigurationProperties.getSendBufferConfig());
		putProperty(props, ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, consumerConfigurationProperties.getSessionTimeoutMsConfig());
		putProperty(props, ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, consumerConfigurationProperties.getValueDeserializerClassConfig());
		putProperty(props, ConsumerConfig.VALUE_DESERIALIZER_CLASS_DOC, consumerConfigurationProperties.getValueDeserializerClassDoc());
		
		return props;
	}
	
	private void putProperty(Properties props, String key, String value) {
		if(StringUtils.isBlank(value)) {
			return;
		}
		props.put(key, value);
	}
	
	@Bean
	public List<String> topics() {
		return consumerConfigurationProperties.getTopics();
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
