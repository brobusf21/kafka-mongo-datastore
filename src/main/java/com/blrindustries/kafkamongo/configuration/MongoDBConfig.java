package com.blrindustries.kafkamongo.configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class MongoDBConfig {
    private static final Logger logger = LoggerFactory.getLogger(MongoDBConfig.class);

    @Value("${mongodb.host}")
    private String host;

    @Value("${mongodb.port}")
    private int port;

    @Value("${mongodb.database}")
    private String database;

    @Value("${mongodb.username}")
    private String username;

    @Value("${mongodb.password}")
    private String password;

    @Value("${mongodb.seeds}")
    private String[] seeds;

    @Value("${mongodb.topiccollectionmap.saletopic}")
    private  String saleTopic;

    @Value("${mongodb.topiccollectionmap.salecollection}")
    private  String salecollection;

    @Value("${mongodb.topiccollectionmap.inventorytopic}")
    private  String inventoryTopic;

    @Value("${mongodb.topiccollectionmap.inventorycollection}")
    private  String invcollection;

    @Bean
    public Map<String, String> collectionMap() {
        Map<String, String> collectionMap = new HashMap<>();
        collectionMap.put(saleTopic,salecollection);
        collectionMap.put(inventoryTopic,invcollection);
        return collectionMap;
    }


    @Bean
    public MongoDatabase mongoDatabase() throws Exception {
        List<MongoCredential> credentials = new ArrayList<>();
        credentials.add(MongoCredential.createCredential(username, "admin", password.toCharArray()));
        MongoClientOptions mongoClientOptions = MongoClientOptions.builder()
                .connectTimeout(30000)
                .maxConnectionIdleTime(10000)
                .connectionsPerHost(20)
                .build();
        MongoClient mongoClient = new MongoClient(getSeeds(), credentials, mongoClientOptions);
        MongoDatabase db = mongoClient.getDatabase(database);
        return db;
    }

    private List<ServerAddress> getSeeds() throws UnknownHostException {
        List<ServerAddress> mongoSeeds = new ArrayList<ServerAddress>();
        for (String host : seeds) {
            mongoSeeds.add(new ServerAddress(host, port));
        }
        return mongoSeeds;
    }

}
