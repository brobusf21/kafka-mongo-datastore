package com.blrindustries.kafkamongo.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;


@Getter @Setter
@Component
@ConfigurationProperties(prefix="odp.kafka")
public class ConsumerConfigurationProperties {

	private String applicationId;
	private String applicationIdDoc;
	private String bootstrapServersConfig;
	private String bufferedRecordsPerPartitionConfig;
	private String clientIdConfig;
	private String commitIntervalMsConfig;
	private String keySerdeClassConfig;
	private String keySerdeClassDoc;
	private String metricsReporterClassesConfig;
	private String metricsNumSamplesConfig;
	private String metricsSampleWindowMsConfig;
	private String numStandbyReplicasConfig;
	private String numStreamThreadsConfig;
	private String partitionGrouperClassConfig;
	private String pollMsConfig;
	private String replicationFactorConfig;
	private String replicationFactorDoc;
	private String stateCleanupDelayMsConfig;
	private String stateDirConfig;
	private String timestampExtractorClassConfig;
	private String valueSerdeClassConfig;
	private String valueSerdeClassDoc;
	private String zookeeperConnectConfig;
	
	private String autoCommitIntervalMsConfig;
	private String autoOffsetResetConfig;
	private String autoOffsetResetDoc;
	private String checkCrcsConfig;
	private String connectionsMaxIdleMsConfig;
	private String defaultExcludeInternalTopics;
	private String defaultMaxPartitionFetchBytes;
	private String enableAutoCommitConfig;
	private String excludeInternalTopicsConfig;
	private String fetchMaxWaitMsConfig;
	private String fetchMinBytesConfig;
	private String groupIdConfig;
	private String heartbeatIntervalMsConfig;
	private String interceptorClassesConfig;
	private String interceptorClassesDoc;
	private String keyDeserializerClassConfig;
	private String keyDeserializerClassDoc;
	private String maxPartitionFetchBytesConfig;
	private String maxPollRecordsConfig;
	private String metadataMaxAgeConfig;
	private String metricReporterClassesConfig;
	private String partitionAssignmentStrategyConfig;
	private String receiveBufferConfig;
	private String reconnectBackoffMsConfig;
	private String requestTimeoutMsConfig;
	private String retryBackoffMsConfig;
	private String sendBufferConfig;
	private String sessionTimeoutMsConfig;
	private String valueDeserializerClassConfig;
	private String valueDeserializerClassDoc;
	
	private List<String> topics;
	
}
