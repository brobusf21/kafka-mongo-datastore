package com.blrindustries.kafkamongo.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties()
public class OdpConfigurationProperties {

	private String url;
	private String token;
}
