package com.blrindustries.kafkamongo;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class MdsOneeightySyncApplicationTests {

	private static final Logger LOGGER              = LoggerFactory.getLogger(MdsOneeightySyncApplicationTests.class);

	private static final String REMOVE_TIME         = "[0-9]{1,2}:[0-9]{1,2}(:[0-9][0-9])?";
	private static final String REGULAR_FORMAT      = "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})";
	private static final String ISO_FORMAT          = "([0-9]{4})-([0-9]{2})-([0-9]{2})";
	private static final String MONTH_DAY_YEAR      = "MM/dd/yy";
	private static final String DAY_MONTH_YEAR      = "dd/MM/yyyy";
	private static final String YEAR_MONTH_DAY      = "yyyy-MM-dd";

	private SimpleDateFormat monthDayYearDateFormat, dayMonthYearDateFormat, yearMonthDayDateFormat;
	private Pattern regDatePattern;
	private Pattern isoDatePattern;

	@Before
	public void init() {
		regDatePattern = Pattern.compile(REGULAR_FORMAT);
		isoDatePattern = Pattern.compile(ISO_FORMAT);
		monthDayYearDateFormat = new SimpleDateFormat(MONTH_DAY_YEAR);
		dayMonthYearDateFormat = new SimpleDateFormat(DAY_MONTH_YEAR);
		yearMonthDayDateFormat = new SimpleDateFormat(YEAR_MONTH_DAY);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void test_success_regularDateConverter() {
		LOGGER.info("BEGIN: test_success_regularDateConverter...");
		String dateExample1 = "01/25/2017";
		String dateExample2 = "1/25/17";
		String dateExample3 = "25/01/2017";
		Matcher dateMatcher1 = regDatePattern.matcher(dateExample1);
		Matcher dateMatcher2 = regDatePattern.matcher(dateExample2);
		Matcher dateMatcher3 = regDatePattern.matcher(dateExample3);
		String dateStr1 = null;
		String dateStr2 = null;
		String dateStr3 = null;
		Date d1;
		Date d2;
		Date d3;
		Long milliseconds1 = new Long(0);
		Long milliseconds2 = new Long(0);
		Long milliseconds3 = new Long(0);

		try {
			if (dateMatcher1.find()) {
				dateStr1 = dateMatcher1.group(0);
				System.out.println(dateStr1);
			}
			if (dateMatcher2.find()) {
				dateStr2 = dateMatcher2.group(0);
			}
			if (dateMatcher3.find()) {
				dateStr3 = dateMatcher3.group(0);
			}
			d1 = monthDayYearDateFormat.parse(dateStr1);
			d2 = monthDayYearDateFormat.parse(dateStr2);
			d3 = dayMonthYearDateFormat.parse(dateStr3);
			milliseconds1 = d1.getTime();
			milliseconds2 = d2.getTime();
			milliseconds3 = d3.getTime();
		} catch (ParseException e) {
			LOGGER.info("ParseException thrown within junit test");
		}
		Assert.assertTrue(milliseconds1.equals(1485324000000L));
		Assert.assertTrue(milliseconds2.equals(1485324000000L));
		Assert.assertTrue(milliseconds3.equals(1485324000000L));
		LOGGER.info("END: test_success_regularDateConverter");
	}

	@Test
	public void test_success_isoDateConverter() {
		LOGGER.info("BEGIN: test_success_isoDateConverter...");
		String dateExample1 = "2017-01-25";
		Matcher dateMatcher1 = isoDatePattern.matcher(dateExample1);
		String dateStr1 = null;
		Date d1;
		Long milliseconds1 = new Long(0);

		try {
			if (dateMatcher1.find()) {
				dateStr1 = dateMatcher1.group(0);
			}
			d1 = yearMonthDayDateFormat.parse(dateStr1);
			milliseconds1 = d1.getTime();
		} catch (ParseException e) {
			LOGGER.info("ParseException thrown; message {} ", e);
		}
		Assert.assertTrue(milliseconds1.equals(1485324000000L));
		LOGGER.info("END: test_success_isoDateConverter");
	}

	@Test
	public void test_success_removeTimeFromDate() {
		LOGGER.info("BEGIN: test_success_removeTimeFromDate...");
		String arrivalDate = "1/25/17 00:00";
		Assert.assertTrue(arrivalDate.replaceAll(REMOVE_TIME, "").trim().equals("1/25/17"));
		LOGGER.info("End: test_success_removeTimeFromDate");
	}

	@Test
	public void isValueNull() {
		LOGGER.info("BEGIN: test_success_isValueNull...");
		Object keyValue = "";
		Assert.assertTrue(keyValue.equals(""));
		LOGGER.info("END: test_success_isValueNull");
	}
}
