# Kafka Mongo Data Store

# Purpose
This application was designed to read from a Kafka topic and store data within a MongoDB instance. As a proof of concept, the data was read from the topic, converted, and then stored.
